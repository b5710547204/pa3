/**
 * Length measurement.
 * @author Patinya Yongyai
 * @version 9.03.2015
 *
 */
public enum Length implements Unit{

	METER("Meter", 1.0),
	KILOMETER("Kilometer",1000.0),
	CENTIMETERS("Centimeter",0.01),
	MILE("Mile",1609.344),
	FOOT("Foot",0.30480),
	WA("Wa",2.0);
	private String name;
	private double value;
	
	Length(String name, double value){
		this.name = name;
		this.value = value;
	}

	/**
	 * Convert from one length unit to another length unit.
	 * @param amt is amount for convert
	 * @param unit is unit to convert
	 * @return value after convert
	 */
	public double convertTo(double amt, Unit unit) {
		return (amt*value)/unit.getValue();
	}

	/**
	 * @return value from old length
	 */
	public double getValue() {
		return this.value;
	}
	
	/**
	 * @return unit from old length
	 */
	public String toString(){
		return this.name;
	}
	
}
