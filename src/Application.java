/**
 * Application to run program.
 * @author Patinya Yongyai
 * @version 9.03.2015
 */
public class Application {
	/**
	 * 
	 * @param args not used
	 */
	public static void main(String[] args) {
		UnitConverter uc = new UnitConverter();
		ConverterUI ui = new ConverterUI(uc);
	}
}
