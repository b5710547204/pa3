/**
 * Weight measurement.
 * @author Patinya Yongyai
 * @version 9.03.2015
 *
 */
public enum Weight implements Unit{
	METRICTON("Metric ton",Math.pow(10, 6)),
	KILOGRAM("Kilogram",1000),
	GRAM("Gram",1.0),
	MILLIGRAM("Milligram",0.001),
	LONGTON("Long ton",1016046.08),
	SHORTTON("Short ton",907184),
	POUND("Pound",453.5922),
	BAHT("Baht",15),
	OUNCE("Ounce",28.3495);
	private String name;
	private double value;
	
	Weight(String name, double value){
		this.name = name;
		this.value = value;
	}

	/**
	 * Convert from one length unit to another length unit.
	 * @param amt is amount for convert
	 * @param unit is unit to convert
	 * @return value after convert
	 */
	public double convertTo(double amt, Unit unit) {
		return (amt*value)/unit.getValue();
	}

	/**
	 * @return value from old length
	 */
	public double getValue() {
		return this.value;
	}
	
	/**
	 * @return unit from old length
	 */
	public String toString(){
		return this.name;
	}

}
