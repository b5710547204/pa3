import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.zip.DataFormatException;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JSeparator;
import javax.swing.JTextField;

/**
 * This is user interface of unit converter.
 * @author Patinya Yongyai
 * @version 9.03.2015
 *
 */
public class ConverterUI extends JFrame implements ActionListener{
	private JTextField inputAmount;
	private JComboBox<Unit> unit1;
	private JLabel equal;
	private JTextField result;
	private JComboBox<Unit> unit2;
	private JButton convert;
	private JButton clear;
	private UnitConverter converter;
	private JMenu menu;
	private JMenuBar menuBar;
	private boolean leftToRight = true;
	private double oldFirstAmount = 0;
	private double oldSecondAmount = 0;
	ConverterUI( UnitConverter uc ){
		super("Unit Converter");
		this.converter = uc;
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		initComponents();
	}

	/**
	 * Compound of ConverterUI class.
	 */
	public void initComponents(){
		Container container = getContentPane();
		container.setLayout(new FlowLayout());
		inputAmount = new JTextField(10);
		inputAmount.addActionListener(this);
		Unit[] items1 = Length.values();
		unit1 = new JComboBox<Unit>(items1);
		equal = new JLabel("=");
		result = new JTextField(10);
		result.addActionListener(this);
		Unit[] items2 = Length.values();
		unit2 = new JComboBox<Unit>(items2);
		convert = new JButton("Convert!");
		convert.addActionListener(this);
		clear = new JButton("Clear");
		clear.addActionListener(new ClearText());

		menuBar = new JMenuBar();
		menu = new JMenu("Unit Type");
		menu.add(new GetLength());

		menu.add(new GetArea());

		menu.add(new GetWeight());

		menu.add(new GetVolume());
		menu.add(new JSeparator());
		menu.add(new Exit());
		menuBar.add(menu);
		this.setJMenuBar(menuBar);

		container.add(inputAmount);
		container.add(unit1);
		container.add(equal);
		container.add(result);
		container.add(unit2);
		container.add(convert);
		container.add(clear);
		pack();
		setVisible(true);
	}

	/**
	 * Show action from UI.
	 * @param e as ActionEvent
	 */
	public void actionPerformed(ActionEvent e) {
		double firstAmount = 0;
		double secondAmount = 0;
		//Set text field font color to black when perform this action.
		inputAmount.setForeground (Color.BLACK);
		result.setForeground(Color.BLACK);
		try{
			//firstAmount and secondAmount will equals zero when no number in text field.
			if(inputAmount.getText().length() != 0)
				firstAmount = Double.parseDouble(inputAmount.getText());
			if(result.getText().length() != 0)
				secondAmount = Double.parseDouble(result.getText());
			//throw exception when both amount are negative number.
			if(firstAmount < 0 || secondAmount < 0)
				throw new DataFormatException();
		}catch(NumberFormatException ex){
			inputAmount.setForeground (Color.red);
			result.setForeground(Color.red);
			JOptionPane.showMessageDialog(ConverterUI.this, "Input only number!!");
		}catch(DataFormatException ex){
			JOptionPane.showMessageDialog(ConverterUI.this, "Input more than 0");
		}finally{
			//If only first text field changed and not equals zero OR same value that occurs when combobox has been changed OR only first text field not equals zero but second text field equals zero.
			if((firstAmount != oldFirstAmount && secondAmount == oldSecondAmount && firstAmount!=0) || (firstAmount == oldFirstAmount && secondAmount == oldSecondAmount && leftToRight) || (firstAmount != 0 && secondAmount==0)){
				Unit fromUnit = (Unit)unit1.getSelectedItem();
				Unit toUnit = (Unit)unit2.getSelectedItem();
				secondAmount = converter.convert(firstAmount,fromUnit,toUnit);
				result.setText(secondAmount+"");
				leftToRight = true;
			//Opposite with above one.
			}else if((firstAmount == oldFirstAmount && secondAmount != oldSecondAmount && secondAmount !=0) || (firstAmount == oldFirstAmount && secondAmount == oldSecondAmount && !leftToRight) || (secondAmount != 0 && firstAmount==0)){
				Unit fromUnit = (Unit)unit2.getSelectedItem();
				Unit toUnit = (Unit)unit1.getSelectedItem();
				firstAmount = converter.convert(secondAmount, fromUnit, toUnit);
				inputAmount.setText(firstAmount+"");
				leftToRight = false;
			}
			//Updating the old value to compare in next time.
			oldFirstAmount = firstAmount;
			oldSecondAmount = secondAmount;
		}
		
	}

	/**
	 * Clear text in text field.
	 * @author Patinya Yongyai
	 *
	 */
	public class ClearText implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			inputAmount.setText("");
			result.setText("");
		}

	}

	/**
	 * Get length measurement from Length class.
	 * @author Patinya Yongyai
	 *
	 */
	public class GetLength extends AbstractAction{
		/**
		 * Set title of Length class.
		 */
		public GetLength(){
			super("Length");
		}
		/**
		 * @param e is event from user
		 */
		public void actionPerformed(ActionEvent e){
			inputAmount.setText("");
			result.setText("");
			UnitType utype = UnitType.Length;
			setItemInComboBox(utype);
		}
	}

	/**
	 * Get area measurement from Area class.
	 * @author Patinya Yongyai
	 *
	 */
	public class GetArea extends AbstractAction{
		/**
		 * Set title of Area class.
		 */
		public GetArea(){
			super("Area");
		}
		/**
		 * @param e is event from user
		 */
		public void actionPerformed(ActionEvent e){
			inputAmount.setText("");
			result.setText("");
			UnitType utype = UnitType.Area;
			setItemInComboBox(utype);
		}
	}

	/**
	 * Get weight measurement from Weight class.
	 * @author Patinya Yongyai
	 *
	 */
	public class GetWeight extends AbstractAction{
		/**
		 * Set title of Weight class.
		 */
		public GetWeight(){
			super("Weight");
		}
		/**
		 * @param e is event from user
		 */
		public void actionPerformed(ActionEvent e){
			inputAmount.setText("");
			result.setText("");
			UnitType utype = UnitType.Weight;
			setItemInComboBox(utype);
		}
	}

	/**
	 * Get volume measurement from Volume class.
	 * @author Patinya Yongyai
	 *
	 */
	public class GetVolume extends AbstractAction{
		/**
		 * Set title of Volume class.
		 */
		public GetVolume(){
			super("Volume");
		}
		/**
		 * @param e is event from user
		 */
		public void actionPerformed(ActionEvent e){
			inputAmount.setText("");
			result.setText("");
			UnitType utype = UnitType.Volume;
			setItemInComboBox(utype);
		}
	}
	
	/**
	 * 
	 * @param utype is unit type from selected by user
	 */
	public void setItemInComboBox(UnitType utype){
		// get all the Length units from the UnitConverter
		Unit[] units = converter.getUnits(utype);

		// populate the user interface with these units
		unit1.removeAllItems();
		unit2.removeAllItems();
		for (Unit u : units)
			unit1.addItem(u);
		for (Unit u : units)
			unit2.addItem(u);
		clear.doClick();
		pack();
	}

	/**
	 * Exit from program.
	 * @author Patinya Yongyai
	 *
	 */
	public class Exit extends AbstractAction{
		/**
		 * Set title of Exit class.
		 */
		public Exit(){
			super("Exit");
		}
		/**
		 * @param e is event from user
		 */
		public void actionPerformed(ActionEvent e){
			System.exit(1);
		}
	}
}
