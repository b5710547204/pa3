/**
 * Convert value from from one length unit to another length unit.
 * @author Patinya Yongyai
 * @version 9.03.2015
 *
 */
public class UnitConverter {
	
	/**
	 * 
	 * @param amount is value for convert
	 * @param fromUnit is old unit
	 * @param toUnit is new unit
	 * @return value after convert
	 */
	public double convert(double amount,Unit fromUnit,Unit toUnit){
		return fromUnit.convertTo(amount, toUnit);
	}
	
	/**
	 * @param utype is type of unit
	 * @return value of unit type
	 */
	public Unit[] getUnits(UnitType utype){
		switch (utype){
		case Length:
			return Length.values();
		case Area:
			return Area.values();
		case Weight:
			return Weight.values();
		case Volume:
			return Volume.values();
		}
		return null;
	}

}
