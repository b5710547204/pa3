/**
 * Area measurement.
 * @author Patinya Yongyai
 * @version 9.03.2015
 *
 */
public enum Area implements Unit{
	SQUAREKILOMETER("Square kilomater",Math.pow(10, 6)),
	SQUAREMETER("Square meter",1.0),
	HECTARE("Hectare",Math.pow(10, 4)),
	SQUAREMILE("Square mile",2589990),
	ACRE("Acre",4046.8564224),
	SQUAREYARD("Square yard",0.83612736),
	TARANGWA("Tarang wa",4.0),
	SQUAREFOOT("Square foot",0.09290304),
	SQUAREINCH("Square inch",0.00064516);
	private String name;
	private double value;
	
	Area(String name, double value){
		this.name = name;
		this.value = value;
	}

	/**
	 * Convert from one length unit to another length unit.
	 * @param amt is amount for convert
	 * @param unit is unit to convert
	 * @return value after convert
	 */
	public double convertTo(double amt, Unit unit) {
		return (amt*value)/unit.getValue();
	}

	/**
	 * @return value from old length
	 */
	public double getValue() {
		return this.value;
	}
	
	/**
	 * @return unit from old length
	 */
	public String toString(){
		return this.name;
	}

}
