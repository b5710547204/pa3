/**
 * Volume measurement.
 * @author Patinya Yongyai
 * @version 9.03.2015
 *
 */
public enum Volume implements Unit{
	LITER("liter",0.001),
	MILLILITER("Milliliter",0.000001),
	CUBICCENTIMETER("Cubic centimeter",0.000001),
	CUBICFOOT("Cubic foot",0.0283168466),
	CUBICMETER("Cubic meter",1.0),
	THANG("Thang",20);
	private String name;
	private double value;
	
	Volume(String name, double value){
		this.name = name;
		this.value = value;
	}

	/**
	 * Convert from one length unit to another length unit.
	 * @param amt is amount for convert
	 * @param unit is unit to convert
	 * @return value after convert
	 */
	public double convertTo(double amt, Unit unit) {
		return (amt*value)/unit.getValue();
	}

	/**
	 * @return value from old length
	 */
	public double getValue() {
		return this.value;
	}
	
	/**
	 * @return unit from old length
	 */
	public String toString(){
		return this.name;
	}
}
