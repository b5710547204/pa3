/**
 * Which class used this interface, that class must used every method in this interface.
 * @author Patinya Yongyai
 * @version 9.03.2015
 *
 */
public interface Unit {
	
	/**
	 * 
	 * @param amt is amount for convert
	 * @param unit unit to convert
	 * @return value after convert
	 */
	double convertTo(double amt,Unit unit);
	/**
	 * 
	 * @return value from old length
	 */
	double getValue();
	/**
	 * 
	 * @return unit from old length
	 */
	String toString();

}
