/**
 * Unit type in this program.
 * @author Patinya Yongyai
 * @version 9.03.2015
 *
 */
public enum UnitType {
	Length,
	Weight,
	Area,
	Volume;
}
